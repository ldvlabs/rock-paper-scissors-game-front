import { Opponent } from './opponent';

export const SELECTABLE_OPPONENTS: Opponent[] = [
  { id: 1, name: 'local' },
  { id: 2, name: 'remote' }
];
