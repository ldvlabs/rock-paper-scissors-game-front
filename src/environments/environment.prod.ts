export const environment = {
  production: true
};

export const serverlessUrl = 'https://4s9wed9e65.execute-api.eu-west-1.amazonaws.com/mvp/play';
export const serverUrl = 'https://lucadv-rock-paper-scissors.herokuapp.com/play';
